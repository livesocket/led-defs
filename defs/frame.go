package defs

// Frame Represents a collection of Effects that the driver
// should execute
type Frame struct {
	Effects []*Effect `json:"effects"`
	Loop    uint64    `json:"loop"`
}

// NewFrame Creates a new Frame
func NewFrame(effects []*Effect, loop uint64) *Frame {
	return &Frame{
		Effects: effects,
		Loop:    loop,
	}
}
