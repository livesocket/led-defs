package defs

// Effect Represents an effect that the driver should
// execute
type Effect struct {
	Name string                 `json:"name"`
	Args map[string]interface{} `json:"args"`
	Loop uint64                 `json:"loop"`
}

// NewEffect Creates a new Effect
func NewEffect(name string, args map[string]interface{}, loop uint64) *Effect {
	return &Effect{
		Name: name,
		Args: args,
		Loop: loop,
	}
}
