package defs

import (
	"encoding/json"
)

// Animation Represents an collection of frames that the
// led driver should execute
type Animation struct {
	Frames   []*Frame `json:"frames"`
	Loop     uint64   `json:"loop"`
	Override bool     `json:"override"`
}

// NewAnimation Creates a new Animation
func NewAnimation(frames []*Frame, loop uint64, override bool) *Animation {
	return &Animation{
		Frames:   frames,
		Loop:     loop,
		Override: override,
	}
}

// ToJSON Converts the Animation to json for transport
func (a *Animation) ToJSON() ([]byte, error) {
	j, err := json.Marshal(a)
	if err != nil {
		return nil, err
	}
	return j, nil
}

// AnimationFromJSON Converts a json string to Animation
func AnimationFromJSON(j string) (*Animation, error) {
	var animation Animation
	err := json.Unmarshal([]byte(j), &animation)
	if err != nil {
		return nil, err
	}
	return &animation, nil
}
